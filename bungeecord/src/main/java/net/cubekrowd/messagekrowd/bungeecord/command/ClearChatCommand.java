package net.cubekrowd.messagekrowd.bungeecord.command;

import java.util.HashMap;
import java.util.Map;
import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdBungeeCordPlugin;
import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdSettings;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ClearChatCommand extends Command {

    private final MessageKrowdBungeeCordPlugin plugin;
    private final BaseComponent[] CLEAR = TextComponent.fromLegacyText(new String(new char[100]).replace('\0', '\n'));

    public ClearChatCommand(MessageKrowdBungeeCordPlugin plugin) {
        super("clearchat", "messagekrowd.clearchat");
        this.plugin = plugin;
    }

    private void sendClear(MessageKrowdSettings settings, ServerInfo si) {
        for (var online : si.getPlayers()) {
            online.sendMessage(CLEAR);
            online.sendMessage(TextComponent.fromLegacyText(
                    plugin.prefix + ChatColor.translateAlternateColorCodes('&',
                            settings.chatClearedFormat)));
        }
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(ChatColor.RED + "This is a player-only command");
            return;
        }
        ProxiedPlayer pp = ((ProxiedPlayer) sender);
        var ppServer = pp.getServer().getInfo();
        var settings = plugin.globalSettings;
        var group = settings.getServerGroupByServer(ppServer);

        if (group.servers.size() > 1) {
            logClearChat(pp, "g_" + group.name);
            for (var si : group.servers) {
                if (si != null) {
                    sendClear(settings, si);
                }
            }
        }

        // search for a group did not return the function which means no result
        logClearChat(pp, "s_" + ppServer.getName());
        sendClear(settings, ppServer);
    }

    private void logClearChat(ProxiedPlayer pp, String target) {
        if (plugin.hasESAPI) {
            Map<String, String> data = new HashMap<>();
            data.put("u", pp.getUniqueId().toString());
            data.put("t", target);
            EventStorageAPI.getStorage().addEntry(new EventEntry(
                    plugin.getDescription().getName(), "clearchat", data));
        }
    }

}

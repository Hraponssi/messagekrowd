package net.cubekrowd.messagekrowd.bungeecord;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;

public final class AprilFools {
    private static final Pattern COLOR_PATTERN = Pattern.compile("(§[0-9a-fklmnor])");

    private AprilFools() {}

    public static boolean isAprilFools() {
        var now = LocalDateTime.now();
        if (now.getMonth() != Month.APRIL) return false;
        if (now.getDayOfMonth() > 2) return false;
        if (now.getDayOfMonth() == 1) return true;
        if (now.getDayOfMonth() == 2 && now.getHour() < 12) return true;

        return false;
    }

    public static String reverse(String s) {
        // Also reverses color codes, so we need to do this more smartly.
        // We will split at color code boundaries to produce alternating
        // text and color groups, e.g.
        // [text0, color0, text1, color1, ..., text[N-1], color[N-1], text[N]]
        // where some of the text can be empty strings, and some of the colors
        // can be null.

        var texts = COLOR_PATTERN.split(s, -1);
        var colors = new String[texts.length - 1];
        var matcher = COLOR_PATTERN.matcher(s);
        String lastColor = null;
        for (int i = 0; i < colors.length; i++) {
            // If we find a color, write it. Otherwise, we need to propagate
            // colors forward, 
            if (matcher.find()) {
                colors[i] = matcher.group();
                lastColor = colors[i];
            } else {
                colors[i] = lastColor;
            }
        }

        // and then reverse the order of the groups. Note that the last
        // element in the colors array should not be put at the beginning,
        // as it's supposed to be a suffix color.
        var builder = new StringBuilder();
        for (int i = 1; i <= texts.length; i++) {
            var color = (i < texts.length) ? colors[texts.length - i - 1] : null;
            var text = texts[texts.length - i];
            if (color != null) builder.append(color);
            builder.append(new StringBuilder(text).reverse().toString());
        }

        // apply suffix color
        if (colors.length > 0 && colors[colors.length-1] != null) {
            builder.append(colors[colors.length-1]);
        }

        return builder.toString();
    }

}
